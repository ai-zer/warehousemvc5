﻿using System.Collections.Generic;
using WarehouseMvc.Models;
using WarehouseMvc.Models.Category;
using WarehouseMvc.Models.Product;

namespace WarehouseMvc.ViewModels.Product
{
    /// <summary>
    /// Модель для представления всех товаров в категории
    /// </summary>
    public class ProductsOfCategoryViewModel
    {
        /// <summary>
        /// Parent category
        /// </summary>
        public TopCategory ParentCategory { get; set; }
        /// <summary>
        /// Products of parent category
        /// </summary>
        public List<ProductModel> Products { get; set; }


        /// <summary>
        /// Количество товаров данной категории
        /// </summary>
        public int TotalCount { get; set; }
    }
}