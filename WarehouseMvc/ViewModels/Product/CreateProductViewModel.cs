﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using WarehouseMvc.Models;
using WarehouseMvc.Models.Category;
using WarehouseMvc.Models.Product;

namespace WarehouseMvc.ViewModels.Product
{
    /// <summary>
    /// Модель для представления  Product.Create
    /// </summary>
    public class CreateProductViewModel
    {
        /// <summary>
        /// Элемент списка категорий(key:value)
        /// </summary>
        public class CategorySelectItem
        {
            public int CategoryId { get; set; }
            public string CategoryName { get; set; }
        }

        /// <summary>
        /// Список категорий для выбора
        /// </summary>
        public List<CategorySelectItem> Categories { get; set; }


        public ProductModel Product { get; set; }

        //[Display(Name = "Изображение товара")]
        //[FileExtensions(Extensions = "jpg",  ErrorMessage = "Выберите JPG файл размером не более 600х600 пикселей")]
        //public HttpPostedFileBase ImageFile { get; set; }


        #region Operations

        public static List<CategorySelectItem> ConvertToSelectItem(List<TopCategory> models)
        {
            //Mapper.CreateMap<UserRoleElement, UserRole>();
            //UserRole uRole = Mapper.Map<UserRoleElement, UserRole>(role);
            List<CategorySelectItem> items = new List<CategorySelectItem>();
            foreach (var model in models)
            {
                items.Add(new CategorySelectItem { CategoryId = model.Id, CategoryName = model.Name });
            }
            return items;
        }

        #endregion
    }
}