﻿using System.Collections.Generic;
using WarehouseMvc.Models;

namespace WarehouseMvc.ViewModels.Product
{
    /// <summary>
    /// Модель для представления  Product.Index
    /// </summary>
    public class ProductListViewModel
    {
        public List<ProductViewModel> ProductItems { get; set; }


        /// <summary>
        /// Количество товаров, найденных в БД
        /// </summary>
        public int TotalCount { get; set; }


        public ProductListViewModel()
        {
            ProductItems = new List<ProductViewModel>();
        }
    }
}