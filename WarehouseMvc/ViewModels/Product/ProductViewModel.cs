﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WarehouseMvc.Models;
using WarehouseMvc.Models.Category;
using WarehouseMvc.Models.Product;

namespace WarehouseMvc.ViewModels.Product
{
    /// <summary>
    /// Модель для представления  Product.Details
    ///  Описывает продукт и категорию продукта
    /// </summary>
    public class ProductViewModel
    {

        public TopCategory ParentCategory { get; set; }

        public ProductModel Product { get; set; }
    }
}