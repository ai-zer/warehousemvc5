﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WarehouseMvc.ViewModels.Home
{
    public class HomeIndexViewModel
    {

        public int UserCount { get; set; }


        public int CategoryCount { get; set; }


        public int ProductCount { get; set; }


        public int ProductTotalCount { get; set; }
    }
}