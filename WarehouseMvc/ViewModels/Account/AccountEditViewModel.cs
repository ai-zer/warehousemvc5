﻿using System.ComponentModel.DataAnnotations;

namespace WarehouseMvc.ViewModels.Account
{
    public class AccountEditViewModel
    {
        public int UserId { get; set; }

        [Required(ErrorMessage = "Имя пользователя не должно быть пустым")]
        [Display(Name = "Логин ")]
        public string UserName { get; set; }
        
        [EmailAddress(ErrorMessage = "Введите e-mail адрес вида user@domain.com")]
        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Display(Name = "Описание ")]
        public string Description { get; set; }
    }
}