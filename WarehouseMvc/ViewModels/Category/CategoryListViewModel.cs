﻿using System.Collections.Generic;

namespace WarehouseMvc.ViewModels.Category
{
    /// <summary>
    /// Модель для представления ParentCategory.Index
    /// </summary>
    public class CategoryListViewModel
    {
        /// <summary>
        /// Список категорий
        /// </summary>
        public List<CategoryViewModel> Categories { get; set; }

        /// <summary>
        /// Количество категорий, найденных в базе данных
        /// </summary>
        public int TotalCount { get; set; }


        public CategoryListViewModel()
        {
            Categories = new List<CategoryViewModel>();
        }
    }
}