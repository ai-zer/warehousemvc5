﻿using System.ComponentModel.DataAnnotations;
using WarehouseMvc.Models;

namespace WarehouseMvc.ViewModels.Category
{
    /// <summary>
    /// Модель для таблицы Categories
    /// </summary>
    public class CategoryViewModel 
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть от 2 до 50 символов")]
        [Display(Name = "Название категории")]
        public string Name { get; set; }


        [MaxLength(500, ErrorMessage = "Описание должно быть объёмом не более 500 символов")]
        [Display(Name = "Описание категории")]
        public string Description { get; set; }

        [Display(Name = "Количество товаров в категории")]
        public int ProductCount { get; set; }

    }
}