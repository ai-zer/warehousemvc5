﻿using System.Data.Entity;
using System.IO;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WarehouseMvc.App_Start;
using WarehouseMvc.DAL;
using WarehouseMvc.Utils;

namespace WarehouseMvc
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //AppDomain.CurrentDomain.SetData("DataDirectory", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data"));
            //CheckCustomDirectories();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            AutomapperConfig.RegisterMap();
            Logger.InitLogger();
            Database.SetInitializer<BaseContext>(new DbInitializer());

        }

        private void CheckCustomDirectories()
        {
            if (!Directory.Exists("logs"))
                Directory.CreateDirectory("logs");
            if (!Directory.Exists("~/App_Data/Images"))
                Directory.CreateDirectory("~/App_Data/Images");
        }
    }
}
