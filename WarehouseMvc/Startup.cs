﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WarehouseMvc.Startup))]
namespace WarehouseMvc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
