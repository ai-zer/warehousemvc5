﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Configuration;
using WarehouseMvc.Utils;

namespace WarehouseMvc.Configuration.SectionUserRoles
{
    /// <summary>
    /// Считывает данные из секции "UserRoles" в файле Web.config
    /// </summary>
    public class UserRolesReader
    {
        private static UserRoleSection _rolesConfig =
            ConfigurationManager.GetSection("userRoles") as UserRoleSection;

        /// <summary>
        /// Считывает данные из секции "UserRoles" в файле Web.config
        /// </summary>
        /// <returns>Список ролей, найденных в секции</returns>
        public static List<IdentityRole> GetRolesFromSettings()
        {
            try
            {
                List<IdentityRole> roles = new List<IdentityRole>();

                foreach (UserRoleElement role in _rolesConfig.Roles)
                {
                    // Mapper.Map<UserRoleElement, IdentityRole>(role);
                    IdentityRole uRole = new IdentityRole();
                    uRole.Name = role.Name;
                    roles.Add(uRole);
                }
                return roles;
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в UserRolesReader.GetRolesFromSettings() ", e);
                return null;
            }
        }
    }
}