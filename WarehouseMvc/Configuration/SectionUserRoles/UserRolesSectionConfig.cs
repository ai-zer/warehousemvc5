﻿using System.Configuration;

namespace WarehouseMvc.Configuration.SectionUserRoles
{
    /// <summary>
    /// Описывает элемент  раздела кофигурации
    /// </summary>
    public class UserRoleElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }
    }
    /// <summary>
    /// Описывает перечисление элементов  раздела кофигурации
    /// </summary>
    [ConfigurationCollection(typeof(UserRoleElement))]
    public class UserRoleElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new UserRoleElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((UserRoleElement)element).Name;
        }
    }
    /// <summary>
    /// Описывает раздел конфигурации
    /// </summary>
    public class UserRoleSection : ConfigurationSection
    {
        [ConfigurationProperty("roles", IsDefaultCollection = true)]
        public UserRoleElementCollection Roles
        {
            get { return (UserRoleElementCollection)this["roles"]; }
            set { this["roles"] = value; }
        }
    }
}