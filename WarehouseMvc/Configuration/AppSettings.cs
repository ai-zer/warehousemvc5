﻿using System;
using System.Web.Configuration;
using WarehouseMvc.Utils;

namespace WarehouseMvc.Configuration
{
    /// <summary>
    ///  Класс,  в котром хранятся строковые настройки приложения. Считываются из Web.config
    /// </summary>
    public class AppSettings
    {
        private static int _defaultPageSize = 10;
        private static string _connectionString = string.Empty;


        /// <summary>
        /// Количество строк на странице
        /// </summary>
        public static int PageSize
        {
            get
            {
                try
                {
                    return int.Parse(WebConfigurationManager.AppSettings["PageSize"]);
                }
                catch (Exception e)
                {
                    Logger.Log.Error("Ошибка в AppSettings.PageSize ", e);
                    return _defaultPageSize;
                }
            }
        }

        /// <summary>
        /// Строка подключения к БД 
        /// </summary>
        public static string ConnectionString
        {
            get
            {                              
                    return GetValueByKey("ConnectionString");
            }
        }

        /// <summary>
        /// папка, в которой хранятся логи приложения
        /// </summary>
        public static string LogDirectory
        {
            get
            {
               return GetValueByKey("logsDirectory");
            }
        }

        /// <summary>
        /// папка, в которой хранятся логи приложения
        /// </summary>
        public static string ImageDirectory
        {
            get
            {
                return GetValueByKey("ImageDirectory");
            }
        }

        /// <summary>
        /// Считывает указанный элемент из раздела "AppSettings" в файле  Web.config
        /// </summary>
        /// <param name="key">Название элемента</param>
        /// <returns>Значение элемента</returns>
        private static string GetValueByKey(string key)
        {
            try
            {
                return WebConfigurationManager.AppSettings[key];
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в AppSettings.GetValueByKey() ", e);
                return null;
            }
        }
    }
}