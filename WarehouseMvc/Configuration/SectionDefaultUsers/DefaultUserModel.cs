﻿
namespace WarehouseMvc.Configuration.SectionDefaultUsers
{
    /// <summary>
    /// Модель для хранения данных аккаунта из секции defaultUsers в Web.config
    /// </summary>
    public class DefaultUserModel
    {
        public string UserName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string Role { get; set; }
    }
}