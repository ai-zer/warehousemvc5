﻿using System;
using System.Collections.Generic;
using System.Configuration;
using WarehouseMvc.Utils;

namespace WarehouseMvc.Configuration.SectionDefaultUsers
{
    /// <summary>
    /// Считывает данные из секции "DefaultUsers" в файле Web.config
    /// </summary>
    public class DefaultUsersReader
    {
        private static DefaultUserSection _usersConfig =
            ConfigurationManager.GetSection("defaultUsers") as DefaultUserSection;

        /// <summary>
        /// Считывает данные из секции "DefaultUsers" в файле Web.config
        /// </summary>
        /// <returns>Список пользователей, найденных в секции</returns>
        public static List<DefaultUserModel> GetUsersFromSettings()
        {
            try
            {
                var users = new List<DefaultUserModel>();

                foreach (DefaultUserElement user in _usersConfig.Users)
                {
                    DefaultUserModel acc = new DefaultUserModel();
                    acc.UserName = user.UserName;
                    acc.Email = user.Email;
                    acc.Password = user.Password;
                    acc.Role = user.Role;
                    users.Add(acc);
                }
                return users;
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в DefaultUsersReader.GetUsersFromSettings() ", e);
                return null;
            }
        }
    }
}