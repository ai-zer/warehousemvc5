﻿using System.Configuration;

namespace WarehouseMvc.Configuration.SectionDefaultUsers
{
    /// <summary>
    /// Описывает элемент  раздела кофигурации
    /// </summary>
    public class DefaultUserElement : ConfigurationElement
    {

        [ConfigurationProperty("username", IsKey = true, IsRequired = true)]
        public string UserName
        {
            get { return (string)this["username"]; }
            set { this["username"] = value; }
        }

        [ConfigurationProperty("email", IsKey = true, IsRequired = true)]
        public string Email
        {
            get { return (string)this["email"]; }
            set { this["email"] = value; }
        }

        [ConfigurationProperty("password", IsKey = true, IsRequired = true)]
        public string Password
        {
            get { return (string)this["password"]; }
            set { this["password"] = value; }
        }

        [ConfigurationProperty("role", IsRequired = true)]
        public string Role
        {
            get { return (string)this["role"]; }
            set { this["role"] = value; }
        }
    }
    /// <summary>
    /// Описывает перечисление элементов  раздела кофигурации
    /// </summary>
    [ConfigurationCollection(typeof(DefaultUserElement))]
    public class DefaultUserElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new DefaultUserElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((DefaultUserElement)element).UserName;
        }
    }
    /// <summary>
    /// Описывает раздел конфигурации
    /// </summary>
    public class DefaultUserSection : ConfigurationSection
    {
        [ConfigurationProperty("users", IsDefaultCollection = true)]
        public DefaultUserElementCollection Users
        {
            get { return (DefaultUserElementCollection)this["users"]; }
            set { this["users"] = value; }
        }
    }
}