﻿using System;
using System.Linq;
using System.Threading.Tasks;
using WarehouseMvc.Models;
using WarehouseMvc.Models.Category;
using WarehouseMvc.Utils;

namespace WarehouseMvc.DAL.DAO
{
    public class CategoryDao : BaseDao<TopCategory>
    {

        /// <summary>
        /// Возвращает объект с указанным  Name
        /// </summary>
        /// <param name="name">Name - название категории</param>
        /// <returns>Найденный объект или null  при его отсутствии в наборе данных</returns>
        public virtual TopCategory FindByName(string name)
        {
            try
            {
                return  LocalDbSet.First(x => x.Name == name);
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".FindByName() ", e);
                return null;
            }
        }

        /// <summary>
        /// Возвращает объект с указанным  Name
        /// </summary>
        /// <param name="name">Name - название категории</param>
        /// <returns>Найденный объект или null  при его отсутствии в наборе данных</returns>
        public async Task<TopCategory> FindByNameAsync(string name)
        {
            return FindByName(name);
        }

        /// <summary>
        /// Проверяет объект на уникальность свойства Name
        /// </summary>
        /// <param name="name">Name - название категории</param>
        /// <returns>True, если объект с таким Name найден с наборе, иначе False</returns>
        public async Task<bool> IsUniqueNameAsync(string categoryName)
        {
            var result = await FindByNameAsync(categoryName);
            return (result == null) ? true : false;
        }
    }
}