﻿using System;
using System.Collections.Generic;
using System.Linq;
using WarehouseMvc.Models.Image;
using WarehouseMvc.Utils;

namespace WarehouseMvc.DAL.DAO
{
    public class ProductImageDao : BaseDao<ProductImage>
    {

        public virtual List<ProductImage> FindByParent(int id)
        {                                                       
            try
            {
                return LocalDbSet.Where(x => x.ParentId == id).ToList();
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".FindByParent() ", e);
                return null;
            }
        }
    }
}