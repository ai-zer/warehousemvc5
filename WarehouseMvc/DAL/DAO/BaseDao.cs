﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using PagedList;
using WarehouseMvc.Models;
using WarehouseMvc.Utils;

namespace WarehouseMvc.DAL.DAO
{
    /// <summary>
    /// Базовый объект для доступа к данным
    /// </summary>
    /// <typeparam name="T">Тип объекта</typeparam>
    public class BaseDao<T> where T : BaseModel
    {
        /// <summary>
        /// Главный репозитория для доступа к БД
        /// </summary>
        private BaseContext LocalBaseContext { get; set; }

        /// <summary>
        /// Типизированный репозиторий для доступа к БД
        /// </summary>
        protected DbContext LocalDbContext { get; set; }

        /// <summary>
        /// Набор данных конкректного типа <T>
        /// </summary>
        protected DbSet<T> LocalDbSet { get; set; }

        public BaseDao()
        {
            LocalBaseContext = BaseContext.Create();
            if (LocalBaseContext == null)
                throw new ArgumentNullException("BaseContext not initialized!");
            // приводим главный контекст к необходимому типу
            LocalDbContext = new DbContext(((IObjectContextAdapter)LocalBaseContext).ObjectContext, true);
            LocalDbSet = LocalDbContext.Set<T>();
        }

        
        /// <summary>
        /// Возвращает все объекты в наборе
        /// </summary>
        /// <returns>Список объектов</returns>
        public List<T> FindAll()
        {
            return LocalDbSet.ToList<T>();
        }

        /// <summary>
        /// Возвращает все объекты в наборе
        /// </summary>
        /// <returns>Список объектов</returns>
        public async Task<List<T>> FindAllAsync()
        {
            return await LocalDbSet.ToListAsync<T>();
        }

            
        /// <summary>
        ///  Количество элементов в наборе 
        /// </summary>
        /// <returns></returns>
        public int TotalCount()
        {
            return LocalDbSet.Count();
        }

        public async Task<int> TotalCountAsync()
        {
            return await LocalDbSet.CountAsync();
        }


        /// <summary>
        /// Возвращает объект с указанным ID
        /// </summary>                  
        /// <param name="id">ID объекта</param>
        /// <returns>Найденный объект или null при его отсутствии в наборе</returns>
        public virtual T FindById(int id)
        { 
                return LocalDbSet.Find(id);
        }

        /// <summary>
        /// Возвращает объект с указанным ID
        /// </summary>                  
        /// <param name="id">ID объекта</param>
        /// <returns>Найденный объект или null при его отсутствии в наборе</returns>
        public virtual async Task<T> FindByIdAsync(int id)
        {
                            return await LocalDbSet.FindAsync(id);
        }

        /// <summary>
        /// Добавляет объект в набор
        /// </summary>
        /// <param name="entity">Объект для добавления</param>
        public virtual void Add(T entity)
        {
            try
            {
                LocalDbSet.Add(entity);
                LocalDbContext.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".Add() ", e);
            }
        }

        /// <summary>
        /// Добавляет объект в набор
        /// </summary>
        /// <param name="entity">Объект для добавления</param>
        public virtual async Task AddAsync(T entity)
        {
            try
            {
                LocalDbSet.Add(entity);
                await LocalDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".AddAsync() ", e);
            }
        }

        /// <summary>
        /// Добавляет список объектов  в набор
        /// </summary>
        /// <param name="entities">Объекты для добавления</param>
        public virtual void AddAll(IEnumerable<T> entities)
        {
            try
            {
                LocalDbSet.AddRange(entities);
                LocalDbContext.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".AddAll() ", e);
            }
        }

        /// <summary>
        /// Добавляет список объектов  в набор
        /// </summary>
        /// <param name="entities">Объекты для добавления</param>
        public virtual async Task AddAllAsync(IEnumerable<T> entities)
        {
            try
            {
                LocalDbSet.AddRange(entities);
                await LocalDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".AddAllAsync() ", e);
            }
        }

        public virtual void Update(T entity)
        {
            try
            {
                //3. Mark entities as modified
                LocalBaseContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                //4. call SaveChanges
                LocalBaseContext.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".Update() ", e);
            }
        }

        public virtual async Task UpdateAsync(T entity)
        {
            try
            {
                //3. Mark entities as modified
                LocalBaseContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                //4. call SaveChanges
                await LocalDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".UpdateAsync() ", e);
            }
        }



        public virtual void Delete(int id)
        {
            try
            {
                T entity = FindById(id);
                Delete(entity);
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".Delete() ", e);
            }
        }

        public virtual async Task DeleteAsync(int id)
        {
            try
            {
                T entity = await FindByIdAsync(id);
                await DeleteAsync(entity);
            }
            catch (Exception e)
            {
                Logger.Log.Error(e.Message, e);
            }
        }


        public virtual void Delete(T entity)
        {
            try
            {
                LocalDbSet.Remove(entity);
                LocalBaseContext.SaveChanges();
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".Delete() ", e);
            }
        }

        public virtual async Task DeleteAsync(T entity)
        {
            try
            {
                LocalDbSet.Remove(entity);
                await LocalDbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".DeleteAsync() ", e);
            }
        }

        /// <summary>
        /// Возвращает указанное количество объектов с начала списка (набора данных)
        /// </summary>
        /// <param name="count">Количество возвращаемых объектов</param>
        /// <returns>Список объектов или null</returns>
        public virtual List<T> FindFirstItems(int count)
        {
            try
            {
                return LocalDbSet.Take(count).ToList();
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".FindFirstItems() ", e);
                return null;
            }
        }

        /// <summary>
        /// Возвращает указанное количество объектов с начала списка (набора данных)
        /// </summary>
        /// <param name="count">Количество возвращаемых объектов</param>
        /// <returns>Список объектов или null</returns>
        public virtual async Task<List<T>> FindFirstItemsAsync(int count)
        {
            try
            {
                return await LocalDbSet.Take(count).ToListAsync();
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".FindFirstItemsAsync() ", e);
                return null;
            }
        }

        /// <summary>
        /// Возвращает указанное количество объектов с конца списка (набора данных)
        /// </summary>
        /// <param name="count">Количество возвращаемых объектов</param>
        /// <returns>Список объектов или null</returns>
        public virtual List<T> FindLastItems(int count)
        {
            try
            {
                var listDbSet = LocalDbSet.ToList();
                List<T> miniList = listDbSet.Skip(listDbSet.Count - count).ToList();
                miniList.Reverse();
                return miniList;
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".FindLastItems() ", e);
                return null;
            }

        }

        /// <summary>
        /// Возвращает указанное количество объектов с конца списка (набора данных)
        /// </summary>
        /// <param name="count">Количество возвращаемых объектов</param>
        /// <returns>Список объектов или null</returns>
        public virtual async Task<List<T>> FindLastItemsAsync(int count)
        {
            try
            {
                var listDbSet = await LocalDbSet.ToListAsync();
                List<T> miniList = listDbSet.Skip(listDbSet.Count - count).ToList();
                miniList.Reverse();
                return miniList;
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".FindLastItemsAsync() ", e);
                return null;
            }
        }

        /// <summary>
        /// Возвращает указанное количество объектов постранично 
        /// </summary>
        /// <param name="pageSize">Количество возвращаемых объектов в одной странице</param>
        /// <param name="page">Текущая страница</param>
        /// <returns>Список объектов или null</returns>
        public virtual async Task<IPagedList<T>> FindPagedItemsAsync(int page, int pageSize)
        {
            try
            {
                var listDbSet = LocalDbSet.OrderBy(x => x.Id).ToPagedList(page, pageSize);
                return listDbSet;
                //var listDbSet = LocalDbSet.OrderBy(x => x.Id).Skip((page -1)* pageSize).Take(pageSize);
                //return listDbSet.ToList();
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".FindLastItemsAsync() ", e);
                return null;
            }
        }
    }
}