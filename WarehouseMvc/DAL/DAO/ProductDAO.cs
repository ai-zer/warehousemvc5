﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using WarehouseMvc.Models;
using WarehouseMvc.Models.Product;
using WarehouseMvc.Utils;

namespace WarehouseMvc.DAL.DAO
{
    public class ProductDao : BaseDao<ProductModel>
    {

        public ProductDao() { }

        
        /// <summary>
        /// Возвращает все товары, которые находятся в данной категории
        /// </summary>
        /// <returns>Объекты с указанным Id или null при их отсутствии в наборе</returns>
        public List<ProductModel> FindItemsByCategoryId(int categoryId)
        {
            try
            {
                return LocalDbSet.Where(x => x.CategoryId == categoryId).ToList();
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".FindItemsByCategoryId() ", e);
                return null;
            }
        }

        /// <summary>
        /// Возвращает все товары, которые находятся в данной категории
        /// </summary>
        /// <returns>Объекты с указанным Id или null при их отсутствии в наборе</returns>
        public async Task<List<ProductModel>> FindItemsByCategoryIdAsync(int categoryId)
        {
            return FindItemsByCategoryId(categoryId);
        }


        /// <summary>
        /// Selects the specified number of models in category
        /// </summary>
        /// <param name="count">The amount of the requested models</param>
        /// <returns>The list of selected items or null</returns>
        public async Task<List<ProductModel>> FindFirstItemsByCategoryIdAsync(int count, int categoryId)
        {
            try
            {
                return await LocalDbSet.Where(x => x.CategoryId == categoryId).Take(count).ToListAsync();
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".FindFirstItemsByCategoryIdAsync() ", e);
                return null;
            }
        }

        /// <summary>
        /// Selects the specified number of models in category from the end of the list
        /// </summary>
        /// <param name="count">The amount of the requested models</param>
        /// <returns>The list of selected items or null</returns>
        public async Task<List<ProductModel>> FindLastItemsByCategoryIdAsync(int count, int categoryId)
        {
            try
            {
                var listDbSet = await LocalDbSet.Where(x => x.CategoryId == categoryId).ToListAsync();
                var miniList = listDbSet.Skip(listDbSet.Count - count).ToList();
                miniList.Reverse();
                return miniList;
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".FindLastItemsByCategoryIdAsync() ", e);
                return null;
            }
        }

        /// <summary>
        /// Удалить все товары указанной категории
        /// </summary>
        /// <param name="categoryId">ID категории, у которой необходимо удалить все товары</param>
        /// <returns></returns>
        public virtual async Task DeleteByCategoryAsync(int categoryId)
        {
            try
            {
                var products =await FindItemsByCategoryIdAsync(categoryId);
                foreach(var item in products)
                    await DeleteAsync(item);
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".DeleteByCategoryIdAsync() ", e);
            }
        }
        /// <summary>
        /// Возвращает объект с указанным  Name
        /// </summary>
        /// <param name="name">Name - название товара</param>
        /// <returns>Найденный объект или null  при его отсутствии в наборе данных</returns>
        public virtual async Task<ProductModel> FindByNameAsync(string name)
        {
            try
            {
                return await LocalDbSet.FirstAsync(x => x.Name == name);
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".FindByNameId() ", e);
                return null;
            }
        }

        /// <summary>
        /// Проверяет объект на уникальность свойства Name
        /// </summary>
        /// <param name="name">Name - название товара</param>
        /// <returns>True, если объект с таким Name найден с наборе, иначе False</returns>
        public async Task<bool> IsUniqueNameAsync(string productName)
        {
            var result = await FindByNameAsync(productName);
            return (result == null) ? true : false;
        }
        
    }
}