﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using WarehouseMvc.Configuration;
using WarehouseMvc.Models;
using WarehouseMvc.Models.Category;
using WarehouseMvc.Models.Image;
using WarehouseMvc.Models.Product;

namespace WarehouseMvc.DAL
{
    /// <summary>
    /// Базовый объект для доступа к данным
    /// </summary>
    public class BaseContext : IdentityDbContext<AccountModel>
    {
        /// <summary>
        /// Набор данных для типа ParentCategory
        /// </summary>
        public virtual DbSet<TopCategory> Categories { get; set; }

        /// <summary>
        /// Набор данных для типа ProductModel
        /// </summary>
        public virtual DbSet<ProductModel> Products { get; set; }

        public virtual DbSet<ProductImage> ProductImages { get; set; }    
                                                                           
        //public DbSet<AccountModel> IdentityUsers { get; set; }
                                                                                
        public BaseContext()
            : base(AppSettings.ConnectionString, throwIfV1Schema: false)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Change the name of the table to be Users instead of AspNetUsers
            modelBuilder.Entity<IdentityUser>().ToTable("WhUsers");
            modelBuilder.Entity<AccountModel>().ToTable("WhUsers");    

            modelBuilder.Entity<IdentityRole>().ToTable("WhRoles");
            modelBuilder.Entity<IdentityUserRole>().ToTable("WhUserRoles");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("WhUserClaims");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("WhUserLogins");
        }

        /// <summary>
        /// Возвращает экземпляр класса BaseContext
        /// </summary>
        /// <returns>Экземпляр класса BaseContext</returns>
        public static BaseContext Create()
        {
            return new BaseContext();
        }

    }
}