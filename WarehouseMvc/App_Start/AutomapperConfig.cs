﻿
using AutoMapper;
using WarehouseMvc.Models.Category;
using WarehouseMvc.Models.Product;
using WarehouseMvc.ViewModels.Category;
using WarehouseMvc.ViewModels.Product;

namespace WarehouseMvc.App_Start
{
    public class AutomapperConfig
    {
        public static void RegisterMap()
        {
            Mapper.CreateMap<TopCategory, CategoryViewModel>();

            Mapper.CreateMap<ProductModel, ProductViewModel>();

        }
    }
}