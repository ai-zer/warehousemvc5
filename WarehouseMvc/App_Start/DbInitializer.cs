﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using WarehouseMvc.Configuration.SectionDefaultUsers;
using WarehouseMvc.Configuration.SectionUserRoles;
using WarehouseMvc.DAL;
using WarehouseMvc.Models;
using WarehouseMvc.Utils;

namespace WarehouseMvc.App_Start
{
    public class DbInitializer : MigrateDatabaseToLatestVersion<BaseContext, Migrations.Configuration>
    {
        public override void InitializeDatabase(BaseContext context)
        {
            context.Database.CreateIfNotExists();
            base.InitializeDatabase(context);
            InitializeIdentity(context);
        }

        /// <summary>
        ///     Заполняет базу необходимыми для работы данными - ролями, записями админа.
        /// </summary>
        /// <param name="context">Контекст БД</param>
        private void InitializeIdentity(BaseContext context)
        {
            try
            {
                var userManager = new UserManager<AccountModel>(new UserStore<AccountModel>(context));
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
                //var myinfo = new MyUserInfo() { FirstName = "Pranav", LastName = "Rastogi" };


                var appRoles = UserRolesReader.GetRolesFromSettings();
                if (appRoles != null)
                    CreateRoles(roleManager, appRoles);

                var defaultUsers = DefaultUsersReader.GetUsersFromSettings();
                if (defaultUsers != null)
                    CreateUsers(userManager, roleManager, defaultUsers);


            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".InitializeIdentity() ", e);
            }
        }
        /// <summary>
        /// Создает роли в указанном RoleManager
        /// </summary>
        /// <param name="roleManager">Экземпляр менеджера, в котором создаются роли</param>
        /// <param name="roles">Список ролей для создания</param>
        private void CreateRoles(RoleManager<IdentityRole> roleManager, List<IdentityRole> roles)
        {
            foreach (var role in roles)
            {
                // Create Role  if it does not exist
                if (!roleManager.RoleExists(role.Name))
                {
                    roleManager.Create(role);
                }
            }
        }

        /// <summary>
        /// Добавляет в базу данных пользователей, указанных в Web.config
        /// </summary>
        /// <param name="userManager">Экземпляр менеджера, оперирующего пользователями</param>
        /// <param name="roleManager">Экземпляр менеджера, в котором создаются роли</param>
        /// <param name="users">Промежуточный список пользователей</param>
        private void CreateUsers(UserManager<AccountModel> userManager, RoleManager<IdentityRole> roleManager,
            List<DefaultUserModel> users)
        {
            Mapper.CreateMap<DefaultUserModel, AccountModel>();

            foreach (var user in users)
            {
                var acc = Mapper.Map<DefaultUserModel, AccountModel>(user);
                var result = userManager.Create(acc, user.Password);
                var role = roleManager.FindByName(user.Role);
                if (result.Succeeded)
                {
                    //добавление пользователю роли по умолчанию
                    userManager.AddToRole(acc.Id, role.Name);
                }
            }

        }

    }
}