﻿using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using WarehouseMvc.DAL.DAO;
using WarehouseMvc.ViewModels.Home;

namespace WarehouseMvc.Controllers
{
    public class HomeController : Controller
    {

        /// <summary>
        /// Объект доступа к таблице Products
        /// </summary>
        private ProductDao _productDAO = new ProductDao();

        /// <summary>
        /// Объект доступа к таблице Categories
        /// </summary>
        private CategoryDao _categoryDao = new CategoryDao();

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public async Task<ActionResult> Index()
        {
            var model = new HomeIndexViewModel();
            model.UserCount = UserManager.Users.Count(); 
            model.CategoryCount =await _categoryDao.TotalCountAsync();
            model.ProductCount = await _productDAO.TotalCountAsync();   

            return View(model);
        }

        public ActionResult About()
        {
            return View();
        }
        
    }
}