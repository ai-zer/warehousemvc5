﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WarehouseMvc.Configuration;
using WarehouseMvc.DAL.DAO;
using WarehouseMvc.Models.Category;
using WarehouseMvc.Models.Image;
using WarehouseMvc.Models.Product;
using WarehouseMvc.Utils;
using WarehouseMvc.ViewModels.Product;

namespace WarehouseMvc.Controllers
{
    public class ProductController : Controller
    {
        /// <summary>
        /// Объект доступа к таблице Products
        /// </summary>
        private ProductDao _productDAO = new ProductDao();

        /// <summary>
        /// Объект доступа к таблице Categories
        /// </summary>
        private CategoryDao _categoryDao = new CategoryDao();

        /// <summary>
        /// Объект доступа к таблице Images
        /// </summary>
        private ProductImageDao _productImageDao = new ProductImageDao();



        // GET: /Product/
        [Authorize(Roles = "client, manager, administrator")]
        public async Task<ActionResult> Index()
        {
            ProductListViewModel model = new ProductListViewModel();
            model.TotalCount = await _productDAO.TotalCountAsync();
            List<ProductModel> products = await _productDAO.FindLastItemsAsync(AppSettings.PageSize);
            foreach (var prod in products)
            {
                ProductViewModel pListItem = new ProductViewModel();
                pListItem.Product = prod;
                pListItem.Product.Images = _productImageDao.FindByParent(pListItem.Product.Id);
                /// TODO переделать, чтобы вытянулись одним запросом
                pListItem.ParentCategory = await _categoryDao.FindByIdAsync(prod.CategoryId);
                model.ProductItems.Add(pListItem);
            }
            return View(model);
        }


        // GET: /Product/Details/5
        [Authorize(Roles = "client, manager, administrator")]
        public async Task<ActionResult> Details(int id)
        {
            ProductModel cat = await _productDAO.FindByIdAsync(id);
            return View(cat);
        }

        // GET: /Product/Create
        /// <summary>
        /// Страница создания продукта с возможностью выбрать категорию продукта
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "manager, administrator")]
        public async Task<ActionResult> Create()
        {
            CreateProductViewModel model = new CreateProductViewModel();
            List<TopCategory> categories = await _categoryDao.FindAllAsync();
            model.Categories = CreateProductViewModel.ConvertToSelectItem(categories);
            return View(model);
        }


        // POST: /Product/Create
        [Authorize(Roles = "manager, administrator")]
        [HttpPost]
        public async Task<ActionResult> Create(CreateProductViewModel model)
        {
            List<TopCategory> categories = _categoryDao.FindAll();
            model.Categories = CreateProductViewModel.ConvertToSelectItem(categories);
            try
            {
                if (ModelState.IsValid)
                {
                    // если всё хорошо, смотрим в базу, чтобы исключить объекты с таким же названием
                    if (!await _productDAO.IsUniqueNameAsync(model.Product.Name))
                    {
                        ModelState.AddModelError("Name", "Продукт с таким названием уже существует");
                        return View(model);
                    }
                    LoadPhotos(model.Product, Request);
                    _productDAO.AddAsync(model.Product);
                    _productImageDao.AddAllAsync(model.Product.Images);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".Create() ", e);
                ViewBag.Error = "Ошибка добавления записи";

            }
            return View(model);
        }

        // GET: /Product/CreateProductByCategory
        /// <summary>
        /// Страница создания продукта с уже указанной категорией, которую не изменить на форме
        /// </summary>
        /// <param name="categoryId">ID категории, к которой будет принадлежать продукт</param>
        /// <returns></returns>
        [Authorize(Roles = "manager, administrator")]
        public async Task<ActionResult> CreateProductByCategory(int categoryId)
        {
            ProductViewModel prdVm = new ProductViewModel();
            prdVm.ParentCategory = await _categoryDao.FindByIdAsync(categoryId);
            return View(prdVm);
        }


        // POST: /Product/CreateProductByCategory
        [Authorize(Roles = "manager, administrator")]
        [HttpPost]
        public async Task<ActionResult> CreateProductByCategory(ProductViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // если всё хорошо, смотрим в базу, чтобы исключить объекты с таким же названием
                    if (!await _productDAO.IsUniqueNameAsync(model.Product.Name))
                    {
                        ModelState.AddModelError("Name", "Продукт с таким названием уже существует");
                        return View(model);
                    }
                    LoadPhotos(model.Product, Request);
                    await _productDAO.AddAsync(model.Product);
                    return RedirectToAction("Details", "Category", new { id = model.Product.CategoryId });
                }
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".CreateProductByCategory() ", e);
                ViewBag.Error = "Ошибка добавления записи";
            }
            return View(model);
        }

        //
        // GET: /Product/Edit/5

        [Authorize(Roles = "manager, administrator")]
        public async Task<ActionResult> Edit(int id)
        {
            try
            {
                CreateProductViewModel model = new CreateProductViewModel();
                List<TopCategory> categories = await _categoryDao.FindAllAsync();
                model.Categories = CreateProductViewModel.ConvertToSelectItem(categories);
                model.Product = await _productDAO.FindByIdAsync(id);
                return View(model);
            }
            catch (Exception e)
            {
                Logger.Log.Error(e.Message, e);
                return RedirectToAction("Index");
            }
        }

        //
        // POST: /Product/Edit/5
        [Authorize(Roles = "manager, administrator")]
        [HttpPost]
        public async Task<ActionResult> Edit(CreateProductViewModel model)
        {
            List<TopCategory> categories = _categoryDao.FindAll();
            model.Categories = CreateProductViewModel.ConvertToSelectItem(categories);
            try
            {
                if (ModelState.IsValid)
                {
                    LoadPhotos(model.Product, Request);
                    await _productDAO.UpdateAsync(model.Product);
                    return RedirectToAction("Details", new { id = model.Product.Id });
                }
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".Edit() ", e);
                ViewBag.Error = "Ошибка редактирования записи";
            }
            return View(model);
        }

        //
        // GET: /Product/Delete/5
        [Authorize(Roles = "manager, administrator")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _productDAO.DeleteAsync(id);
            }
            catch (Exception e)
            {
                Logger.Log.Error(e.Message, e);
                ViewBag.Error = "Ошибка удаления записи";
            }
            return RedirectToAction("Index");
        }

        #region Heplers

        private async void LoadPhotos(ProductModel model, HttpRequestBase request)
        {
            var imgList = new List<ProductImage>();
            for (int i = 0; i < request.Files.Count; i++)
            {
                string filePath = null;
                HttpPostedFileBase hpf = request.Files[i];
                if (Path.GetExtension(hpf.FileName) == ".jpg")
                    filePath = ImageManipulator.SaveImage(hpf);

                if (filePath != null)
                {
                    var img = new ProductImage
                    {
                        Path = filePath,
                        CreateTime = DateTime.Now,
                        Name = model.Name + '(' + (i + 1) + ')',
                        ParentId = model.Id
                    };
                    imgList.Add(img);
                    model.Images.Add(img);
                }
            }
            //await _productImageDao.UpdateAllAsync(model.Images);
            //await _productImageDao.AddAllAsync(imgList.ToArray());
        }

        #endregion
    }
}
