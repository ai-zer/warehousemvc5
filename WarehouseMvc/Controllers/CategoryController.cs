﻿using AutoMapper;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using WarehouseMvc.Configuration;
using WarehouseMvc.DAL.DAO;
using WarehouseMvc.Models.Category;
using WarehouseMvc.Utils;
using WarehouseMvc.ViewModels.Category;
using WarehouseMvc.ViewModels.Product;

namespace WarehouseMvc.Controllers
{
    public class CategoryController : Controller
    {
        /// <summary>
        /// Объект доступа к таблице Products
        /// </summary>
        private ProductDao _productDAO = new ProductDao();
        /// <summary>
        /// Объект доступа к таблице Categories
        /// </summary>
        private CategoryDao _categoryDao = new CategoryDao();

        //
        // GET: /ParentCategory/
        [Authorize(Roles = "client, manager, administrator")]
        public async Task<ActionResult> Index(int? currentPage)
        {
            try
            {
                CategoryListViewModel model = new CategoryListViewModel();
                // количество категорий всего
                model.TotalCount = await _categoryDao.TotalCountAsync();
                var viewModels = await _categoryDao.FindLastItemsAsync(AppSettings.PageSize);
                foreach (var cat in viewModels)
                {
                    CategoryViewModel cvm = Mapper.Map<TopCategory, CategoryViewModel>(cat);
                    // указываем, сколько в каждой категории товаров
                    var prods = await _productDAO.FindItemsByCategoryIdAsync(cat.Id);
                    cvm.ProductCount = prods.Count;
                    model.Categories.Add(cvm);
                }
                return View(model);
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".Index() ", e);
                ViewBag.Error = "Ошибка считывания данных";
                return View();
            }
        }


        // GET: /ParentCategory/Details/5
        /// <summary>
        /// Информация о категории, где отображается название, описание и список товаров.
        /// </summary>
        /// <param name="id">ID категории</param>
        /// <returns></returns>
        [Authorize(Roles = "client, manager, administrator")]
        public async Task<ActionResult> Details(int id)
        {
            ProductsOfCategoryViewModel model = new ProductsOfCategoryViewModel();
            model.ParentCategory = await _categoryDao.FindByIdAsync(id);
            model.Products = await _productDAO.FindItemsByCategoryIdAsync(id);

            return View(model);
        }

        //
        // GET: /ParentCategory/Create

        [Authorize(Roles = "manager, administrator")]
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ParentCategory/Create
        [Authorize(Roles = "manager, administrator")]
        [HttpPost]
        public async Task<ActionResult> Create(TopCategory model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // если всё хорошо, смотрим в базу, чтобы исключить объекты с таким же названием 
                    if (!await _categoryDao.IsUniqueNameAsync(model.Name))
                    {
                        ModelState.AddModelError("Name", "Категория с таким названием уже существует");
                        return View();
                    }
                    await _categoryDao.AddAsync(model);
                    return RedirectToAction("Index");
                }
                return View();
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".Create() ", e);
                ViewBag.Error = "Ошибка добавления записи";
                return View();
            }
        }

        //
        // GET: /ParentCategory/Edit/5
        [Authorize(Roles = "manager, administrator")]
        public async Task<ActionResult> Edit(int id)
        {
            try
            {
                TopCategory cat = await _categoryDao.FindByIdAsync(id);
                return View(cat);
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".Edit() ", e);
                return RedirectToAction("Index");
            }
        }

        //
        // POST: /ParentCategory/Edit/5
        [Authorize(Roles = "manager, administrator")]
        [HttpPost]
        public async Task<ActionResult> Edit(TopCategory model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // если всё хорошо, смотрим в базу, чтобы исключить объекты с таким же названием 
                    await _categoryDao.UpdateAsync(model);
                    return RedirectToAction("Details", new { id = model.Id });
                }
                return View();
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".Edit() ", e);
                ViewBag.Error = "Ошибка редактирования записи";
                return View();
            }
        }

        //
        // GET: /ParentCategory/Delete/5
        [Authorize(Roles = "manager, administrator")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {   // удалить продукты этой категории
                await _productDAO.DeleteByCategoryAsync(id);
                // удалить категорию
                await _categoryDao.DeleteAsync(id);
            }
            catch (Exception e)
            {
                Logger.Log.Error("Ошибка в " + this.ToString() + ".Delete() ", e);
                ViewBag.Error = "Ошибка удаления записи";
            }
            return RedirectToAction("Index");
        }

        #region Heplers



        #endregion
    }
}
