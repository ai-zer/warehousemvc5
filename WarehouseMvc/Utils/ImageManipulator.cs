﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Web;
using WarehouseMvc.Configuration;

namespace WarehouseMvc.Utils
{
    /// <summary>
    /// Класс для работы с изображениями
    /// </summary>
    public class ImageManipulator
    {
        // Максимальные допустимые размеры картинки                        
        private const int MaxWidth = 600;
        private const int MaxHeight = 600;

        /// <summary>
        ///  Сжимает изображение до предельных размеров и созраняет в папке, указанной в настройках приложения
        /// </summary>
        /// <param name="hpf">Загруженный файл,  не более 1Мб </param>
        /// <returns>Относительный путь к файлу, сохранённому в папке с изображениями</returns>
        public static string SaveImage(HttpPostedFileBase hpf)
        {
            if (hpf != null && hpf.ContentLength != 0 && hpf.ContentLength <= 1048576)// 307200)
            {
                using (var originalPic = new Bitmap(hpf.InputStream, false))
                {
                    // Вычисление новых размеров картинки                           
                    var width = originalPic.Width; //текущая ширина                     
                    var height = originalPic.Height; //текущая высота                   
                    var widthDiff = (width - MaxWidth); //разница с допуст. шириной     
                    var heightDiff = (height - MaxHeight); //разница с допуст. высотой    

                    // Определение размеров, которые необходимо изменять                
                    var doWidthResize = (MaxWidth > 0 && width > MaxWidth &&
                                         widthDiff > -1 && widthDiff > heightDiff);
                    var doHeightResize = (MaxHeight > 0 && height > MaxHeight &&
                                          heightDiff > -1 && heightDiff > widthDiff);

                    // Ресайз картинки                                                  
                    if (doWidthResize || doHeightResize || (width.Equals(height) && widthDiff.Equals(heightDiff)))

                    {
                        int iStart;
                        decimal divider;
                        if (doWidthResize)
                        {
                            iStart = width;
                            divider = Math.Abs((decimal) iStart/MaxWidth);
                            width = MaxWidth;
                            height = (int) Math.Round((height/divider));
                        }
                        else
                        {
                            iStart = height;
                            divider = Math.Abs((decimal) iStart/MaxHeight);
                            height = MaxHeight;
                            width = (int) Math.Round((width/divider));
                        }
                    }

                    // Сохраняем файл в папку пользователя                             
                    using (var newBmp = new Bitmap(originalPic, width, height))
                    {
                        using (var oGraphics = Graphics.FromImage(newBmp))
                        {
                            oGraphics.SmoothingMode = SmoothingMode.AntiAlias;
                            oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

                            oGraphics.DrawImage(originalPic, 0, 0, width, height);
                            var ext = Path.GetExtension(hpf.FileName);
                            // Формируем имя для картинки                           
                            var imageName = Guid.NewGuid();
                            var relpath = AppSettings.ImageDirectory + imageName + ext;
                            var filePath =
                                HttpContext.Current.Server.MapPath(relpath);

                            if (File.Exists(filePath))
                                File.Delete(filePath);

                            newBmp.Save(filePath);
                            return relpath;
                        }
                    }
                }
            }
            return null;
        }
    }
}