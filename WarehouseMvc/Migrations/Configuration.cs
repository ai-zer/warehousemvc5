namespace WarehouseMvc.Migrations
{
    using System.Data.Entity.Migrations;

    /// <summary>
    /// ������������ ��� �������� ��������
    /// </summary>
    public sealed class Configuration : DbMigrationsConfiguration<WarehouseMvc.DAL.BaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "WarehouseMvc.DAL.BaseContext";
        }

        protected override void Seed(WarehouseMvc.DAL.BaseContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
