namespace WarehouseMvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _2 : DbMigration
    {
        public override void Up()
        {

            // old tables
            CreateTable(
                "dbo.Projects",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    CategoryId = c.Int(nullable: false),
                    Name = c.String(nullable: false, maxLength: 100),
                    Description = c.String(maxLength: 500),
                    Link = c.String(),
                    Path = c.String(),
                    Image = c.String(),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.DownloadFiles",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    FileName = c.String(nullable: false),
                    FilePath = c.String(nullable: false),
                    SaveDate = c.DateTime(nullable: false),
                    Description = c.String(),
                    OwnerId = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.ArticleCategories",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(nullable: false, maxLength: 100),
                    Description = c.String(maxLength: 500),
                    ItemsCount = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.HashTags",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Value = c.String(),
                    Article_Id = c.Int(),
                    DownloadFile_Id = c.Int(),
                    Project_Id = c.Int(),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Articles",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    CategoryId = c.Int(nullable: false),
                    Name = c.String(nullable: false, maxLength: 100),
                    Description = c.String(maxLength: 500),
                    Content = c.String(),
                    Image = c.String(),
                    ArticleCategory_Id = c.Int(),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Accounts",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    UserName = c.String(nullable: false),
                    Password = c.String(nullable: false),
                    Role = c.Int(nullable: false),
                    Email = c.String(),
                })
                .PrimaryKey(t => t.Id);
        }
        
        public override void Down()
        {
        }
    }
}
