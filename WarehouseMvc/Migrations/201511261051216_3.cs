namespace WarehouseMvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _3 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Users", newName: "WarehouseAccounts");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.WarehouseAccounts", newName: "Users");
        }
    }
}
