namespace WarehouseMvc.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.WarehouseAccounts", newName: "WarehouseUsers");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.WarehouseUsers", newName: "WarehouseAccounts");
        }
    }
}
