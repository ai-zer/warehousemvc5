﻿
using System.ComponentModel.DataAnnotations.Schema;

namespace WarehouseMvc.Models.Image
{
    [Table("WhProductImages")]
    public class ProductImage : BaseImage
    {

        public int ParentId { get; set; }
    }
}