﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace WarehouseMvc.Models.Image
{

    public abstract class BaseImage : BaseModel
    {

        //[Required]
        [Display(Name = "Название изображения")]
        public string Name { get; set; }


        [Display(Name = "Путь к файлу")]
        public string Path { get; set; }

        [Display(Name = "Дата создания")]
        public DateTime CreateTime { get; set; }
    }
}