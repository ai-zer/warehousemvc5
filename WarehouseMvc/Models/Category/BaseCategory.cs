﻿using System.ComponentModel.DataAnnotations;
using System.Configuration;

namespace WarehouseMvc.Models.Category
{
    public abstract class BaseCategory : BaseModel
    {


        [StringValidator(InvalidCharacters = "~!@#$%^&*()[]{}/;'\"|\\")]
        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть от 2 до 50 символов. В названии должны быть только буквы и цифры")]
        [Display(Name = "Название категории")]
        public string Name { get; set; }


        [MaxLength(500, ErrorMessage = "Описание должно быть объёмом не более 500 символов")]
        [Display(Name = "Описание")]
        public string Description { get; set; }
    }
}