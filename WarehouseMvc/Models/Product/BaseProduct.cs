﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using WarehouseMvc.Models.Image;

namespace WarehouseMvc.Models.Product
{
    public abstract class BaseProduct : BaseModel
    {


        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [Display(Name = "Категория товара")]
        public int CategoryId { get; set; }

        [StringValidator(InvalidCharacters = "~!@#$%^&*()[]{}/;'\"|\\")]
        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "Длина строки должна быть от 2 до 50 символов. В названии должны быть только буквы и цифры")]
        [Display(Name = "Название")]
        public string Name { get; set; }


        [MaxLength(500, ErrorMessage = "Описание должно быть объёмом не более 500 символов")]
        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Display(Name = "Изображения товара")]
        public virtual List<ProductImage> Images { get; set; }


        protected BaseProduct()
        {
            Images = new List<ProductImage>();
        }
    }
}