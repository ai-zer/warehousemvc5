﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WarehouseMvc.Models.Product
{
    /// <summary>
    /// Описывает сущность "Товар"
    /// </summary>
    [Table("WhProducts")]
    public class ProductModel : BaseProduct
    {
        [Display(Name = "Стоимость единицы")]
        public int Cost { get; set; }

        [Display(Name = "Количество на складе")]
        public int Count { get; set; }
    }
}